import { createStore, useStore as baseUseStore, Store } from 'vuex'
import { InjectionKey } from 'vue'
import gallery, { GalleryState } from './modules/gallery'

export interface RootState {
  gallery: GalleryState
}

export const key: InjectionKey<Store<RootState>> = Symbol('Vuex')

export const store = createStore({
  modules: {
    gallery
  }
})

// define your own `useStore` composition function
export function useStore() {
  return baseUseStore(key)
}
