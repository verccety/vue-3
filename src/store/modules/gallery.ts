import getPhotos from '@/composables/getPhotos'
import Photo from '@/types/Photo'
import { ActionTree, ActionContext, MutationTree } from 'vuex'

// initial state
// shape: [{ id, quantity }]
const state = {
  photos: [] as Photo[],
  currentPage: 1,
  totalPages: null as number | null,
  limit: 10,
  isLoading: false,
  error: null as null | string
}
export type GalleryState = typeof state

// getters
const getters = {}

// actions
const actions: ActionTree<GalleryState, GalleryState> = {
  async fetchPhotos({ commit }) {
    if (state.totalPages && state.currentPage > state.totalPages) {
      return
    }
    try {
      commit('setIsLoading', true)
      const { photos, totalCount } = await getPhotos(String(state.currentPage))

      commit('setPhotos', photos)
      commit('setTotalCount', Number(totalCount))
      commit('setCurrentPage')
    } catch (error) {
      commit('setIsLoading', false)
      commit('setPhotos', [])
      commit('setError', error)
    } finally {
      commit('setIsLoading', false)
    }
  }
}

// mutations
const mutations: MutationTree<GalleryState> = {
  setPhotos(state: GalleryState, data: Photo[]) {
    state.photos = [...state.photos, ...data]
  },

  setIsLoading(state: GalleryState, status: boolean) {
    state.isLoading = status
  },
  setError(state: GalleryState, error: string) {
    state.error = error
  },
  setTotalCount(state: GalleryState, count: number) {
    state.totalPages = Math.ceil(Number(count) / state.limit)
  },
  setCurrentPage(state: GalleryState) {
    state.currentPage += 1
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
