import { ref } from 'vue'

const getPost = (id: number) => {
  const error = ref(null)
  const post = ref(null)

  const load = async () => {
    try {
      const data = await fetch('http://localhost:3000/posts/' + id)
      if (!data.ok) {
        throw Error('that post does not exist')
      }
      post.value = await data.json()
    } catch (err) {
      error.value = err.message
      console.log(error.value)
    }
  }

  return { post, error, load }
}

export default getPost
