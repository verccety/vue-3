import { ref } from 'vue'

//* custom hooks alternative
const getPosts = () => {
  const error = ref(null)
  const posts = ref([])

  const load = async () => {
    const data = await fetch('http://localhost:3000/posts')
    try {
      if (!data.ok) {
        throw Error('no data available')
      }
      posts.value = await data.json()
    } catch (err) {
      error.value = err.message
      console.log(error.value)
    }
  }

  return { posts, error, load }
}

export default getPosts
