import Photo from '@/types/Photo'

const getPosts = async (start = '0', limit = '10') => {
  let error
  let photos: Photo[]
  let totalCount
  const data = await fetch(
    'https://jsonplaceholder.typicode.com/photos?' +
      new URLSearchParams({
        _start: start,
        _limit: limit
      })
  )

  try {
    if (!data.ok) {
      throw Error('no data available')
    }
    photos = await data.json()
    totalCount = data.headers.get('x-total-count')
  } catch (err) {
    error = err.message
    throw Error(error)
  }

  return { photos, totalCount }
}

export type getPostsResponse = ReturnType<typeof getPosts>

export default getPosts
