import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './assets/global.css'
import { store, key } from './store'
import directives from './directives/index'
const app = createApp(App)

for (const directive of directives) {
  app.directive(directive.name, directive)
}
app
  .use(router)
  .use(store, key)
  .mount('#app')
