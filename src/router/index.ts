import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Jobs from '../views/jobs/Jobs.vue'
import JobsDetails from '../views/jobs/JobsDetails.vue'
import NotFound from '../views/NotFound.vue'
import ReactionTimer from '../components/ReactionTimer/ReactionTimer.vue'
import SignupForm from '../components/Signup/SignupForm.vue'
import CompositionApi from '../views/CompositionApi.vue'
import SinglePostDetails from '../components/Composition/SinglePostDetails.vue'
import HyreleJobs from '../views/hyrele-jobs/HyreleJobs.vue'
import PhotoGallery from '@/views/photo-gallery/PhotoGallery.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/jobs',
    name: 'Jobs',
    component: Jobs
  },
  {
    path: '/jobs/:id',
    name: 'JobsDetails',
    component: JobsDetails,
    // to access router param as a prop
    props: true
  },
  {
    path: '/posts/:id',
    name: 'SinglePostDetails',
    component: SinglePostDetails,
    props: true
  },
  {
    path: '/hyrele',
    name: 'HyreleJobs',
    component: HyreleJobs
  },
  {
    path: '/reaction-timer',
    name: 'ReactionTimer',
    component: ReactionTimer
  },
  {
    path: '/signup',
    name: 'SignupForm',
    component: SignupForm
  },
  {
    path: '/gallery',
    name: 'PhotoGallery',
    component: PhotoGallery
  },
  {
    path: '/composition',
    name: 'CompositionApi',
    component: CompositionApi
  },
  // redirect
  { path: '/all-jobs', redirect: '/jobs' },
  // catch 404 errors
  {
    path: '/:catchAll(.*)',
    name: 'NotFound',
    component: NotFound
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
